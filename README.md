# example-nginx-deploy-ansible

This project provides Ansible configurations to deploy a new nginx version on AWS EC2 instances with using dynamic inventory. 

## Technologies

Project is created with:

- Docker
- Ansible
- Nginx
- AWS

## Set Up

To run this project, run this command in Ansible EC2 instance:

    $ ansible-playbook -i demo.aws_ec2.yml nginx.yml --key-file key.pem

## Usage

- demo.aws_ec2.yml: dynamic inventory configuration
- nginx.yml: playbook for deploying new nginx image
- key.pem: private key file to ssh webserver01 and webserver02 (EC2 instances where the new version will be installed)